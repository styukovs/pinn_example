nbviewer link: https://nbviewer.org/urls/gitlab.com/styukovs/pinn_example/-/raw/master/pinn_example/differential_equation.ipynb/%3Fflush_cache%3Dtrue

Example is taken from Appendix A. Data-driven solution of partial differential equations in https://doi.org/10.1016/j.jcp.2018.10.045
